/*
 ============================================================================
 Name        : lab1.3.c
 Author      : Muravyeva Alena
 Version     :
 Copyright   :
 Description : The program  transforms degrees to radians and Vice versa
 ============================================================================
 */


#include <stdio.h>


int main()
{
   char letter='D';
   const float pi=3.14;
   float value =0;

   setlinebuf(stdout);

   printf("Hello.Enter the  value of the angle\n");
   scanf("%f %c",&value,&letter);

   if(letter=='D')
   {
      letter='R';
      printf("%.2f%c\n", (value*pi)/180, letter);
   }
   else if (letter=='R')
   {
      letter='D';
      printf("%.2f%c\n", (value*180)/pi, letter);
   }
   else
   {
      printf("Error. Enter, pleas correct data\n ");
   }
   return 0;
}
